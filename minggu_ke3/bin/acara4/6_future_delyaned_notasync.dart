Future delaPrint(int seconds, String message) {
  final durasi = Duration(seconds: seconds);
  return Future.delayed(durasi).then((value) => message);
}

main(List<String> args) {
  print("Roger");
  delaPrint(2, "Bajak Laut").then((status) {
    print(status);
  });
  print("is");
}
