void main(List<String> args) async {
  print("Come on every body sing");
  print(await lirik1());
  print(await lirik2());
  print(await lirik3());
  print(await lirik4());

}

Future<String> lirik1() async {
  String first = "Dear god........";
  return Future.delayed(Duration(seconds: 4), () => (first));
}

Future<String> lirik2() async {
  String second = "The only thing i ask of you";
  return Future.delayed(Duration(seconds: 3), () => (second));
}

Future<String> lirik3() async {
  String third = "Is to hold her when i'm not around";
  return Future.delayed(Duration(seconds: 2), () => (third));
}

Future<String> lirik4() async {
  String fourth = "When i'm much to far away";
  return Future.delayed(Duration(seconds: 2), () => (fourth));
}
