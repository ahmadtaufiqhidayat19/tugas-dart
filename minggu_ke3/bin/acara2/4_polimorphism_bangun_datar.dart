void main(List<String> args) {
  bangunDatar BDAR = bangunDatar();
  persegi Persegi = new persegi(6.4);
  lingkaran Bunder = new lingkaran(22, 3.14);

  // BDAR.luas();
  // BDAR.keliling();
  print("Keliling Persegi : ${Persegi.keliling()}");
  print("Luas Persegi : ${Persegi.luas()}");
  print("Keliling Bunder : ${Bunder.keliling()}");
  print("Luas Bunder : ${Bunder.luas()}");
}

class bangunDatar {
  double luas() {
    return 0;
  }

  double keliling() {
    return 0;
  }
}

class persegi extends bangunDatar {
  double sisi = 0;
  persegi(double sisi) {
    this.sisi = sisi;
  }

  @override
  double luas() {
    return this.sisi * sisi;
  }

  @override
  double keliling() {
    return 4 * this.sisi;
  }
}

class lingkaran extends bangunDatar {
  double r2 = 0;
  double phi = 0;

  lingkaran(double r2, phi) {
    this.r2;
    this.phi;
  }

  @override
  double luas() {
    return phi * r2 * r2;
  }

  @override
  double keliling() {
    2 * phi * r2;
    return 0;
  }
}
