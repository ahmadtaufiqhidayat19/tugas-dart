void main(List<String> args) {
  human manusia = new human();
  attackTitan eren = new attackTitan();

  manusia.pPoint = 80;
  eren.pPoint = 77;

  titan t1 = human();
  titan t2 = attackTitan();

  print("Manusia power point : ${manusia.pPoint}");
  print("Attack titan power point : ${eren.pPoint}");

  print("Manusia : " + (t1 as human).bunuhTitan());
  print("Eren : " + (t2 as attackTitan).serang());
}

class titan {
  int _pPoint = 0;
  int get pPoint => _pPoint;
  set pPoint(int power) {
    if (power <= 5) {
      power = 5;
    }
    _pPoint = power;
  }
}

class human extends titan {
  String bunuhTitan() => "sasageyo";
}

class attackTitan extends titan {
  String serang() => "blam blam blam. pengerasan, kuncian, bela diri";
}
