void main(List<String> args) {
  var data1 = Employee.nik(1234567);
  var data2 = Employee.name("Achmad Taufiq Hidayat");
  var data3 = Employee.departemen("Kominfo");
  print("NIK : " + data1.nik.toString());
  print("Nama : " + data2.name);
  print("Departemen : " + data3.departemen);
}

class Employee {
  int nik = 0;
  String name = "";
  String departemen = "";

  Employee.nik(this.nik);
  Employee.name(this.name);
  Employee.departemen(this.departemen);
}
