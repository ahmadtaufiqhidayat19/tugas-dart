void main(List<String> args) {
  //bramching sederhana
  var tokoStatus = "buka";
  if (tokoStatus == 'buka') {
    print('saya mau belanja');
  } else {
    print('toko tutup');
  }

  //branching dengan kondisi
  var tokoStatus2 = "tutup";
  var cutdownToko = 5;
  if (tokoStatus2 == 'buka') {
    print('saya mau belanja telur dan buah');
  } else if (cutdownToko >= 5) {
    print('sebentar lagi buka, saya tunggu');
  } else {
    print('toko tutup, saya tidak jadi beli');
  }
}
